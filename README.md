# README #

Application for taking selfies with unity on iOS, using a native extension.

*/CameraAccess* contains the native-ios-plugin xcode project, implementing unity's
low level rendering interface (though not really making use of it), in platform independent format in c++ and objective c++ (iOS implementation).

*/SelfieCam* contains the unity project using the camera access plugin.

There are three buttons, **start**, **snap**(take picture) and **stop**.
When started the texture of the background panel turns into a live feed from the native iOS camera.
Take picture natively stores the pic in the apps document folder(constant name so gets overwritten every time). The pic is then loaded from unity and shown in another small texture at the top left.

PluginActivator.mm in the *Plugin/iOS* dir registers the low level rendering intrface by extending UnityAppController.
The XCode project should copy the lib into the *Plugin/iOS* after build.

Functions exposed by the plugin:

	void CameraAccessStart (string gameObjectName, string methodName);

	void CameraAccessStop ();

	IntPtr CameraAccessGetCameraFeedTexture ();

	int CameraAccessGetTextureWidth ();

	int CameraAccessGetTextureHeight ();

	void CameraAccessTakePicture ();

	IntPtr GetRenderEventFunc();

**gameObjectName** describes an object with a method **methodName**, that will take the url of the recently taken pic (via CameraAccessTakePicture()):

	public void methodName(string url)

Used the following samples and references:

Unity:
https://docs.unity3d.com/ScriptReference/Texture2D.CreateExternalTexture.html
https://docs.unity3d.com/Manual/NativePluginInterface.html
https://docs.unity3d.com/Manual/PluginsForIOS.html

https://bitbucket.org/Unity-Technologies/graphicsdemos
https://bitbucket.org/Unity-Technologies/iosnativecodesamples/branch/5.5-dev

iOS:
https://developer.apple.com/library/content/samplecode/MetalVideoCapture/Introduction/Intro.html
https://developer.apple.com/library/content/samplecode/AVCam/Introduction/Intro.html

Don't forget to add 

    <key>NSCameraUsageDescription</key>
    <string>Take awesome Pics!</string>
    
to the plist.