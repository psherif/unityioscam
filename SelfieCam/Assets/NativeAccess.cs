﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using System.Runtime.InteropServices;
using System.IO;

public class NativeAccess : MonoBehaviour
{
	
	//native interface access function
	#if UNITY_IPHONE && !UNITY_EDITOR
	[DllImport ("__Internal")]
	#endif
	private static extern void CameraAccessStart (string gameObjectName, string methodName);

	#if UNITY_IPHONE && !UNITY_EDITOR
	[DllImport ("__Internal")]
	#endif
	private static extern void CameraAccessStop ();

	#if UNITY_IPHONE && !UNITY_EDITOR
	[DllImport ("__Internal")]
	#endif
	private static extern IntPtr CameraAccessGetCameraFeedTexture ();

	#if UNITY_IPHONE && !UNITY_EDITOR
	[DllImport ("__Internal")]
	#endif
	private static extern int CameraAccessGetTextureWidth ();

	#if UNITY_IPHONE && !UNITY_EDITOR
	[DllImport ("__Internal")]
	#endif
	private static extern int CameraAccessGetTextureHeight ();

	#if UNITY_IPHONE && !UNITY_EDITOR
	[DllImport ("__Internal")]
	#endif
	private static extern void CameraAccessTakePicture ();

	#if UNITY_IPHONE && !UNITY_EDITOR
	[DllImport ("__Internal")]
	#endif
	private static extern IntPtr GetRenderEventFunc();

	int width = -1;
	int height = -1;

	private bool cameraAccessActive = false;

	private bool takePic = false;

	private RawImage cameraFeed;

	private CanvasGroup cameraFeedGroup;

	private RawImage pictureTaken;

	void Awake ()
	{  
		cameraFeed = this.GetComponent<RawImage> ();
		cameraFeedGroup = cameraFeed.GetComponent<CanvasGroup>();
		GameObject pictureGO = GameObject.Find ("Picture");
		pictureTaken = pictureGO.GetComponent<RawImage> ();
		pictureTaken.gameObject.SetActive (false);
		cameraFeedGroup.alpha = 0;
	}

	// Use this for initialization
	void Start ()
	{
		StartCoroutine (EndOfFrameUpdate ());
	}


	public void StartCamera ()
	{
		//cameraFeed.gameObject.SetActive (true);
		cameraAccessActive = true;
		//start camerafeed, pass name of object and method for picture taken callback (via UnitySendMessage)
		CameraAccessStart ("CameraFeed", "PictureTaken");

		cameraFeedGroup.alpha = 1;
	}

	public void StopCamera ()
	{
		CameraAccessStop ();
		cameraAccessActive = false;
		cameraFeedGroup.alpha = 0;

	}

	public void TakePicture ()
	{
		takePic = true;
	}

	//UnitySendMessage target 
	public void PictureTaken(string url){
		Debug.Log ("PictureTaken called with : " + url);
		if (String.IsNullOrEmpty (url)) {
			Debug.Log ("invalid string!");
			return;
		}
		byte[] fileData;
		if (File.Exists(url))     {
			fileData = File.ReadAllBytes(url);
			Texture2D tex  =   new Texture2D(2, 2, TextureFormat.RGBA32, false);//Texture2D tex = new Texture2D (2, 2, TextureFormat.BGRA32,false);
			bool success = tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
			if (success)
				pictureTaken.texture = tex;
			else
				Debug.Log ("Couldn't load image!");

		}else {
			StartCoroutine (LoadImgWithWWW (url));
		}


		//cameraFeed.SizeToParent(9f);
		resizeToAspectRatio (pictureTaken);
		pictureTaken.gameObject.SetActive (true);
	}

	private IEnumerator LoadImgWithWWW(string url){
		Texture2D tex  =   new Texture2D(2, 2, TextureFormat.RGBA32, false);
		WWW www = new WWW(url);
		yield return www;
		www.LoadImageIntoTexture(tex);

		pictureTaken.texture = tex;
	}
	
	// Update is called once per frame
	void Update ()
	{
	}

	private IEnumerator EndOfFrameUpdate ()
	{
		while (true) {
			yield return new WaitForEndOfFrame ();
		
			// Issue a plugin event with arbitrary integer identifier.
			// The plugin can distinguish between different
			// things it needs to do based on this ID.
			// For our simple plugin, it does not matter which ID we pass here.
			//GL.IssuePluginEvent(GetRenderEventFunc(), 1);

			try {
				if (takePic) {
					//Call to native TakePicture, to store pic in gallery and send back message with url
					CameraAccessTakePicture();
					takePic = false;
				}
				if (cameraAccessActive) {
					if (cameraFeed.texture == null) {
						performTextureSetup ();
					} else {
						IntPtr nativeTexture = CameraAccessGetCameraFeedTexture();

						((Texture2D)cameraFeed.texture).UpdateExternalTexture (nativeTexture);
					}
				
				} else if (cameraFeed.texture != null) {
					Destroy (cameraFeed.texture );
					cameraFeed.texture = null;
					//nativeTexture = IntPtr.Zero;
				}
			} catch (Exception e) {
				Debug.Log ("Error! " + e);
			}
		}
	}

	private void  performTextureSetup ()
	{
		Debug.Log ("performTextureSetup");

		IntPtr nativeTexture = CameraAccessGetCameraFeedTexture ();
		width = CameraAccessGetTextureWidth ();
		height = CameraAccessGetTextureHeight ();
		if (width == 0 || height == 0)
			return;
		cameraFeed.texture = Texture2D.CreateExternalTexture (width, height, TextureFormat.BGRA32, false, true, nativeTexture);

		Debug.Log (" *** performTextureSetup width : " + width + " height : " + height + " tex : " + cameraFeed.texture);

		resizeToAspectRatio (cameraFeed);

	}

	private void resizeToAspectRatio(RawImage img){
		
		Canvas canvas = GameObject.Find ("Canvas").GetComponent<Canvas> (); 
		RectTransform rt = img.GetComponent<RectTransform> ();
		int width = Mathf.RoundToInt (rt.rect.width * canvas.scaleFactor);
		int height = Mathf.RoundToInt (rt.rect.height * canvas.scaleFactor);

		float aspect = (float)img.texture.height /  (float)img.texture.width;

		height = (int)((float)width * aspect);

		rt.SetSizeWithCurrentAnchors( RectTransform.Axis.Vertical, height);
	}


}
