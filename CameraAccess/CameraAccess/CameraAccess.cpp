//
//  CameraAccess.mm
//  CameraAccess
//
//  Created by Patric Sherif on 28/01/2017.
//  Copyright © 2017 Patric Sherif. All rights reserved.
//
// Example low level rendering Unity plugin

#include <string.h>
#include "PlatformBase.h"
#include "CameraAPI.h"

#include <assert.h>

//active api
static CameraAPI* s_CurrentAPI = NULL;

//store gameobject and method name for callback
static char *   g_gameObjectName  = NULL;
static char *   g_methodName = NULL;

//declaration for function to send messages to unity
extern "C" void UnitySendMessage(const char *, const char *, const char *);

//declare log print function
void PrintNativeLog(const char* text);

// Helper function to create C string copy
char* CAmakeStringCopy (const char* string)
{
    if (string == NULL)
        return NULL;
    
    char* res = (char*)malloc(strlen(string) + 1);
    strcpy(res, string);
    return res;
}

//Callback function for api when picture has been saved to disk

static void PictureTakenCallback(const char* url){
    if(g_gameObjectName == NULL || g_methodName == NULL || url == NULL){
        return;
    }
    PrintNativeLog("PictureTakenCallback");
    //PrintNativeLog(g_gameObjectName);
    //PrintNativeLog(g_methodName);
    UnitySendMessage(g_gameObjectName, g_methodName, CAmakeStringCopy(url));
}

//Interface functions to be called by unity, calling through to the cpp implementation
extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API CameraAccessStart(char* gameObjectName, char* methodName){
    PrintNativeLog("CameraAccessStart");
    //PrintNativeLog(gameObjectName);
    //PrintNativeLog(methodName);
    g_gameObjectName = CAmakeStringCopy(gameObjectName);
    g_methodName = CAmakeStringCopy(methodName);
    s_CurrentAPI->StartCameraFeed(PictureTakenCallback);
}

extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API CameraAccessStop(){
    s_CurrentAPI->StopCameraFeed();
}

extern "C" void* UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API CameraAccessGetCameraFeedTexture(){
    return s_CurrentAPI->GetCameraFeedTexture();
}

extern "C" int UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API  CameraAccessGetTextureWidth(){
    int w = s_CurrentAPI->GetTextureWidth();
    return w;
}

extern "C" int UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API  CameraAccessGetTextureHeight(){
    
    int h = s_CurrentAPI->GetTextureHeight();
    return h;}

extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API  CameraAccessTakePicture(){
    s_CurrentAPI->TakePicture();
}

// --------------------------------------------------------------------------
// UnitySetInterfaces

static void UNITY_INTERFACE_API OnGraphicsDeviceEvent(UnityGfxDeviceEventType eventType);

static IUnityInterfaces* s_UnityInterfaces = NULL;
static IUnityGraphics* s_Graphics = NULL;

extern "C" void	UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API UnityPluginLoad(IUnityInterfaces* unityInterfaces)
{
    
    PrintNativeLog("UnityPluginLoad");
    s_UnityInterfaces = unityInterfaces;
    s_Graphics = s_UnityInterfaces->Get<IUnityGraphics>();
    s_Graphics->RegisterDeviceEventCallback(OnGraphicsDeviceEvent);
    
    // Run OnGraphicsDeviceEvent(initialize) manually on plugin load
    OnGraphicsDeviceEvent(kUnityGfxDeviceEventInitialize);
}

extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API UnityPluginUnload()
{
    
    PrintNativeLog("UnityPluginUnload");
    s_Graphics->UnregisterDeviceEventCallback(OnGraphicsDeviceEvent);
}

// --------------------------------------------------------------------------
// GraphicsDeviceEvent

static UnityGfxRenderer s_DeviceType = kUnityGfxRendererNull;

static void UNITY_INTERFACE_API OnGraphicsDeviceEvent(UnityGfxDeviceEventType eventType)
{
    PrintNativeLog("OnGraphicsDeviceEvent");
    // Create graphics API implementation upon initialization
    if (eventType == kUnityGfxDeviceEventInitialize)
    {
        assert(s_CurrentAPI == NULL);
        s_DeviceType = s_Graphics->GetRenderer();
        s_CurrentAPI = CreateCameraAPI(s_DeviceType);
        
        assert(s_CurrentAPI != NULL);
    }
    
    // Let the implementation process the device related events
    if (s_CurrentAPI)
    {
        s_CurrentAPI->ProcessDeviceEvent(eventType, s_UnityInterfaces);
    }
    
    // Cleanup graphics API implementation upon shutdown
    if (eventType == kUnityGfxDeviceEventShutdown)
    {
        delete s_CurrentAPI;
        s_CurrentAPI = NULL;
        s_DeviceType = kUnityGfxRendererNull;
    }
}

// --------------------------------------------------------------------------
// OnRenderEvent
// This will be called for GL.IssuePluginEvent script calls; eventID will
// be the integer passed to IssuePluginEvent. In this example, we just ignore
// that value.

static void UNITY_INTERFACE_API OnRenderEvent(int eventID)
{
    //PrintNativeLog("OnRenderEvent");
    // Unknown / unsupported graphics device type? Do nothing
    if (s_CurrentAPI == NULL)
        return;
    
}

// --------------------------------------------------------------------------
// GetRenderEventFunc, an example function we export which is used to get a rendering event callback function.

extern "C" UnityRenderingEvent UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API GetRenderEventFunc()
{
    return OnRenderEvent;
}



