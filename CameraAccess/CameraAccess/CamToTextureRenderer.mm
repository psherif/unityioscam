
#import "CamToTextureRenderer.h"

#import <QuartzCore/CAMetalLayer.h>
#import <Metal/Metal.h>
#import <UIKit/UIKit.h>

#import <UIKit/UIDevice.h>
//#import <simd/simd.h>
#import <CoreVideo/CVMetalTextureCache.h>
#import "AssetsLibrary/AssetsLibrary.h"

@implementation CamToTextureRenderer
{
    id <MTLDevice> _device;
    
    // this value will cycle from 0 to g_max_inflight_buffers whenever a display completes ensuring renderer clients
    // can synchronize between g_max_inflight_buffers count buffers, and thus avoiding a constant buffer from being overwritten between draws
    //NSUInteger _constantDataBufferIndex;
    
    // Video texture
    AVCaptureSession *_captureSession;
    CVMetalTextureCacheRef _videoTextureCache;
    id <MTLTexture> _videoTexture;
    
    CallbackFunctionPtr _callback;
    //id <MTLTexture> _videoTexture[2];
}

- (instancetype)init
{
    self = [super init];
    /*if (self) {
        _constantDataBufferIndex = 0;
    }*/
    return self;
}

- (void)configure:(CallbackFunctionPtr)callback{
    
    NSLog(@"******* CamRenderer Configure *********");
    
    // initialize and load all necessary data for video texture
    _callback = callback;
    _device = MTLCreateSystemDefaultDevice();
    [self setupVideoTexture];
}

- (void)start{
    [_captureSession startRunning];
}

- (void)stop{
    [_captureSession stopRunning];
}

- (id<MTLTexture>) getImageAsTexture{
    
    //NSLog(@"CamRenderer getImageAsTexture");
    return _videoTexture/*[_constantDataBufferIndex]*/;
}

- (void) savePictureToCameraRoll{
    
    UIImage* image = [CamToTextureRenderer imageWithMTLTexture:_videoTexture];// [UIImage imageWithCIImage:cimage];
    
    
    NSData *imageData = UIImagePNGRepresentation(image);

    //NSLog(@"image %@", imageData);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *imagePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",@"selfieCam"]];
    
    NSLog(@"the image Path is %@",imagePath);
    
    NSLog(@"pre writing to file");
    if (![imageData writeToFile:imagePath atomically:NO])
    {
        NSLog(@"Failed to save image data to disk");
    }
    
    if(_callback && imagePath){
        const char* url=[imagePath UTF8String];
        _callback(url);
    }
}

static void MBEReleaseDataCallback(void *info, const void *data, size_t size)
{
    free((void *)data);
}

+ (UIImage *)imageWithMTLTexture:(id<MTLTexture>)texture
{
    NSLog(@"texture format :%lu ",(unsigned long)[texture pixelFormat]);
    
    CGSize imageSize = CGSizeMake([texture width], [texture height]);
    size_t imageByteCount = imageSize.width * imageSize.height * 4;
    void *imageBytes = malloc(imageByteCount);
    NSUInteger bytesPerRow = imageSize.width * 4;
    MTLRegion region = MTLRegionMake2D(0, 0, imageSize.width, imageSize.height);
    [texture getBytes:imageBytes bytesPerRow:bytesPerRow fromRegion:region mipmapLevel:0];
    
    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, imageBytes, imageByteCount, MBEReleaseDataCallback);
    int bitsPerComponent = 8;
    int bitsPerPixel = 32;
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CGBitmapInfo bitmapInfo = kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big;
    CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
    
    CGImageRef imageRef = CGImageCreate(imageSize.width,
                                        imageSize.height,
                                        bitsPerComponent,
                                        bitsPerPixel,
                                        bytesPerRow,
                                        colorSpaceRef,
                                        bitmapInfo,
                                        provider,
                                        NULL,
                                        false,
                                        renderingIntent);
    
    imageRef = [CamToTextureRenderer CreateRGBAImageWithBGRAImage:imageRef];
    
    float angle = 180.0 * (M_PI/180.0);

    UIImage *image = [UIImage imageWithCGImage:imageRef scale:0.0 orientation:UIImageOrientationUp];
    
    image = [CamToTextureRenderer rotateImage: image byRadian: angle];
    
    CFRelease(provider);
    CFRelease(colorSpaceRef);
    CFRelease(imageRef);
    
    return image;
}

+ (UIImage *)rotateImage:(UIImage*)src byRadian:(CGFloat)radian
{
    // calculate the size of the rotated view's containing box for our drawing space
    CGRect rect = CGRectApplyAffineTransform(CGRectMake(0,0, src.size.width, src.size.height), CGAffineTransformMakeRotation(radian));
    CGSize rotatedSize = rect.size;
    
    // Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    //   // Rotate the image context
    CGContextRotateCTM(bitmap, radian);
    
    // Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-src.size.width / 2, -src.size.height / 2, src.size.width, src.size.height), [src CGImage]);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (CGImageRef) CreateRGBAImageWithBGRAImage: (CGImageRef) abgrImageRef {
    // the data retrieved from the image ref has 4 bytes per pixel (ABGR).
    CFDataRef abgrData = CGDataProviderCopyData(CGImageGetDataProvider(abgrImageRef));
    UInt8 *pixelData = (UInt8 *) CFDataGetBytePtr(abgrData);
    long length = CFDataGetLength(abgrData);
    
    // bgra to rgba
    // swap the blue and red components for each pixel...
    UInt8 tmpByte = 0;
    for (int index = 0; index < length; index+= 4) {
        tmpByte = pixelData[index + 0];
        pixelData[index + 0] = pixelData[index + 2];
        pixelData[index + 2] = tmpByte;
    }
    
    // grab the bgra image info
    size_t width = CGImageGetWidth(abgrImageRef);
    size_t height = CGImageGetHeight(abgrImageRef);
    size_t bitsPerComponent = CGImageGetBitsPerComponent(abgrImageRef);
    size_t bitsPerPixel = CGImageGetBitsPerPixel(abgrImageRef);
    size_t bytesPerRow = CGImageGetBytesPerRow(abgrImageRef);
    CGColorSpaceRef colorspace = CGImageGetColorSpace(abgrImageRef);
    CGBitmapInfo bitmapInfo = CGImageGetBitmapInfo(abgrImageRef);
    
    // create the rgba image
    CFDataRef argbData = CFDataCreate(NULL, pixelData, length);
    CGDataProviderRef provider = CGDataProviderCreateWithCFData(argbData);
    CGImageRef argbImageRef = CGImageCreate(width, height, bitsPerComponent, bitsPerPixel, bytesPerRow,
                                            colorspace, bitmapInfo, provider, NULL, true, kCGRenderingIntentDefault);
    
    // release what we can
    CFRelease(abgrData);
    CFRelease(argbData);
    CGDataProviderRelease(provider);
    
    // return the pretty new image
    return argbImageRef;
}


- (void)setupVideoTexture
{
    NSLog(@"CamRenderer setupVideoTexture");
    CVMetalTextureCacheFlush(_videoTextureCache, 0);
    CVReturn textureCacheError = CVMetalTextureCacheCreate(kCFAllocatorDefault, NULL, _device, NULL, &_videoTextureCache);
    
    if (textureCacheError)
    {
        NSLog(@">> ERROR: Couldnt create a texture cache");
        assert(0);
    }
    
    // Make and initialize a capture session
    _captureSession = [[AVCaptureSession alloc] init];
    
    if (!_captureSession) {
        NSLog(@">> ERROR: Couldnt create a capture session");
        assert(0);
    }
    
    [_captureSession beginConfiguration];
    [_captureSession setSessionPreset:AVCaptureSessionPresetPhoto];
    
    // Get the a video device with preference to the front facing camera
    AVCaptureDevice* videoDevice = nil;
    
    
    NSArray<NSString *> *deviceTypes = @[AVCaptureDeviceTypeBuiltInWideAngleCamera, AVCaptureDeviceTypeBuiltInDualCamera, AVCaptureDeviceTypeBuiltInTelephotoCamera];
    
    NSArray* devices = [[AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:deviceTypes mediaType:AVMediaTypeVideo position:AVCaptureDevicePositionFront] devices];
    //NSArray* devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    
    for (AVCaptureDevice* device in devices)
    {
        if ([device position] == AVCaptureDevicePositionFront)
        {
            videoDevice = device;
        }
    }
    
    if(videoDevice == nil)
    {
        videoDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    }
    
    if(videoDevice == nil)
    {
        NSLog(@">> ERROR: Couldnt create a AVCaptureDevice");
        assert(0);
    }
    
    NSError *error;
    
    // Device input
    AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:&error];
    
    if (error)
    {
        NSLog(@">> ERROR: Couldnt create AVCaptureDeviceInput");
        assert(0);
    }
    
    [_captureSession addInput:deviceInput];
    
    // Create the output for the capture session.
    AVCaptureVideoDataOutput * dataOutput = [[AVCaptureVideoDataOutput alloc] init];
    
    NSArray* bla = [dataOutput availableVideoCVPixelFormatTypes];
    
    NSLog(@"pixel formats %@", bla);
    
   
    [dataOutput setAlwaysDiscardsLateVideoFrames:YES];
    
    // Set the color space.
    [dataOutput setVideoSettings:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA]
                                                             forKey:(id)kCVPixelBufferPixelFormatTypeKey]];
    
    // Set dispatch to be on the main thread to create the texture in memory and allow Metal to use it for rendering
    [dataOutput setSampleBufferDelegate:self queue:dispatch_get_main_queue()];
    
    [_captureSession addOutput:dataOutput];
    
    for (AVCaptureConnection *connection in dataOutput.connections){
        
        [connection setVideoOrientation:AVCaptureVideoOrientationPortraitUpsideDown];
        /*if (connection.supportsVideoOrientation && connection.videoOrientation != AVCaptureVideoOrientationPortrait){
            NSLog(@"Setting Orientation!");
            [connection setVideoOrientation:AVCaptureVideoOrientationPortrait];
        }*/
    }
    
    [_captureSession commitConfiguration];
    
    // this will trigger capture on its own queue
    [_captureSession startRunning];
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    CVReturn error;
    
    CVImageBufferRef sourceImageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    size_t width = CVPixelBufferGetWidth(sourceImageBuffer);
    size_t height = CVPixelBufferGetHeight(sourceImageBuffer);
    
    CVMetalTextureRef textureRef;
    error = CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorDefault, _videoTextureCache, sourceImageBuffer, NULL, MTLPixelFormatBGRA8Unorm, width, height, 0, &textureRef);
    
    if (error)
    {
        NSLog(@">> ERROR: Couldnt create texture from image");
        assert(0);
    }
    
    /*NSUInteger videoCaptureIndex = 0;
    if(_constantDataBufferIndex == 0){
        videoCaptureIndex = 1;
    }*/
    
    _videoTexture/*[videoCaptureIndex]*/ = CVMetalTextureGetTexture(textureRef);
    if (!_videoTexture/*[videoCaptureIndex]*/) {
        NSLog(@">> ERROR: Couldn't get texture from texture ref");
        assert(0);
    }
    //_constantDataBufferIndex = !(_constantDataBufferIndex);
    
    CVBufferRelease(textureRef);

    
    //NSLog(@"******* Texture captured *********");
}



@end
