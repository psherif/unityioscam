#pragma once

#include "IUnityGraphics.h"

#include <stddef.h>

struct IUnityInterfaces;

//callback picturetaken definition
typedef void (*CallbackFunctionPtr)(const char *);

class CameraAPI
{
public:
	virtual ~CameraAPI() { }


	// Process general event like initialization, shutdown, device loss/reset etc.
	virtual void ProcessDeviceEvent(UnityGfxDeviceEventType type, IUnityInterfaces* interfaces) = 0;

    virtual void StartCameraFeed(CallbackFunctionPtr callback) = 0;

    virtual void StopCameraFeed() = 0;
    
    virtual void* GetCameraFeedTexture() = 0;
    
    virtual int GetTextureWidth() = 0;
    
    virtual int GetTextureHeight() = 0;
    
    virtual void TakePicture() = 0;
};


// Create a camera API implementation instance for the given API type.
CameraAPI* CreateCameraAPI(UnityGfxRenderer apiType);

