
#import <Metal/Metal.h>
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreMedia/CoreMedia.h>
#import <Accelerate/Accelerate.h>

//callback function which will receive the photo url after "savePictureToCameraRoll"
typedef void (*CallbackFunctionPtr)(const char *);

@interface CamToTextureRenderer : NSObject <AVCaptureVideoDataOutputSampleBufferDelegate>

// load all assets before triggering rendering
- (void)configure:(CallbackFunctionPtr)callback;

- (void)start;

- (void)stop;

- (id<MTLTexture>) getImageAsTexture;

- (void) savePictureToCameraRoll;

@end
