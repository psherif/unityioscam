#include "CameraAPI.h"
#include "PlatformBase.h"
#include "IUnityGraphics.h"


CameraAPI* CreateCameraAPI(UnityGfxRenderer apiType)
{
    /*
#	if SUPPORT_D3D11
	if (apiType == kUnityGfxRendererD3D11)
	{
	}
#	endif // if SUPPORT_D3D11

#	if SUPPORT_D3D9
	if (apiType == kUnityGfxRendererD3D9)
	{
	}
#	endif // if SUPPORT_D3D9

#	if SUPPORT_D3D12
	if (apiType == kUnityGfxRendererD3D12)
	{
	}
#	endif // if SUPPORT_D3D9


#	if SUPPORT_OPENGL_UNIFIED
	if (apiType == kUnityGfxRendererOpenGLCore || apiType == kUnityGfxRendererOpenGLES20 || apiType == kUnityGfxRendererOpenGLES30)
	{
	}
#	endif // if SUPPORT_OPENGL_UNIFIED

#	if SUPPORT_OPENGL_LEGACY
	if (apiType == kUnityGfxRendererOpenGL)
	{
	}
#	endif // if SUPPORT_OPENGL_LEGACY
    */

#	if SUPPORT_METAL
	if (apiType == kUnityGfxRendererMetal)
	{
		extern CameraAPI* CreateCameraAPI_Metal();
		return CreateCameraAPI_Metal();
	}
#	endif // if SUPPORT_METAL


	// Unknown or unsupported graphics API
	return NULL;
}
