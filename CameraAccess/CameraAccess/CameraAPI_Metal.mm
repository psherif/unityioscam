
#include "CameraAPI.h"
#include "PlatformBase.h"

// Metal implementation of CameraAPI.


#if SUPPORT_METAL
#include "CamToTextureRenderer.h"
#import <Metal/Metal.h>

static CamToTextureRenderer* _renderer = nil;

class CameraAPI_Metal : public CameraAPI
{
public:
	CameraAPI_Metal();
	virtual ~CameraAPI_Metal() { }
	
    virtual void ProcessDeviceEvent(UnityGfxDeviceEventType type, IUnityInterfaces* interfaces);
    
    virtual void StartCameraFeed(CallbackFunctionPtr callback);
    
    virtual void StopCameraFeed();
    
    virtual void* GetCameraFeedTexture();
    
    virtual int GetTextureWidth();
    
    virtual int GetTextureHeight();
    
    virtual void TakePicture();
	
private:
    CamToTextureRenderer* m_renderer = nil;
    
};


CameraAPI* CreateCameraAPI_Metal()
{
	return new CameraAPI_Metal();
}

void PrintNativeLog(const char* text){
    if(text){
        NSLog([NSString stringWithUTF8String: text]);
    }
}

//implementation
CameraAPI_Metal::CameraAPI_Metal()
{
    NSLog(@"CameraAPI constructor");
}

void CameraAPI_Metal::StartCameraFeed(CallbackFunctionPtr callback){
    
    NSLog(@"CameraAPI StartCameraFeed");
    if(m_renderer != nil) return;
    m_renderer = [[CamToTextureRenderer alloc] init];
    [m_renderer configure: callback];
    [m_renderer start];
}

void CameraAPI_Metal::StopCameraFeed(){
    
    NSLog(@"CameraAPI StopCameraFeed");
    if(m_renderer == nil) return;
    [m_renderer stop];
    m_renderer = nil;
}

void* CameraAPI_Metal::GetCameraFeedTexture(){
    
    //NSLog(@"CameraAPI GetCameraFeedTexture");
    return (__bridge void*)[m_renderer getImageAsTexture];
}

int CameraAPI_Metal::GetTextureWidth(){
    
    NSLog(@"CameraAPI GetTextureWidth");
    return (int)[[m_renderer getImageAsTexture] width];
}

int CameraAPI_Metal::GetTextureHeight(){
    
    NSLog(@"CameraAPI GetTextureHeight");
    return (int)[[m_renderer getImageAsTexture] height];
}

void CameraAPI_Metal::TakePicture(){
    
    NSLog(@"CameraAPI TakePicture");
    [m_renderer savePictureToCameraRoll];
}


void CameraAPI_Metal::ProcessDeviceEvent(UnityGfxDeviceEventType type, IUnityInterfaces* interfaces)
{
    
    NSLog(@"CameraAPI ProcessDeviceEvent");
	if (type == kUnityGfxDeviceEventInitialize)
	{
        
	}
	else if (type == kUnityGfxDeviceEventShutdown)
	{
        StopCameraFeed();
	}
}

#endif // #if SUPPORT_METAL
